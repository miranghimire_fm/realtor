package com.fm.realtor.web.api;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fm.realtor.model.House;
import com.fm.realtor.service.HouseService;

@RunWith(SpringRunner.class)
@WebMvcTest
public class HouseControllerTest {

	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	HouseService houseService;
	
	@Test
	public void testGetAllHouse() throws Exception{
		List<House> house = new ArrayList<>();
		house.add(new House("1",512,"Kathmandu",27,10000));
		Mockito.when(houseService.getAllHouse()).thenReturn(house);
		ObjectMapper objectMapper = new ObjectMapper();
		String expected = objectMapper.writeValueAsString(house);
		String URI = "/api/house";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).accept(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		String output = result.getResponse().getContentAsString();
		assertThat(expected).isEqualTo(output);
		
	}
	
	@Test
	public void testGetHouseById() throws Exception{
		House house = new House("1",512,"Kathmandu",27,10000);
		Mockito.when(houseService.getHouseById(Mockito.anyString())).thenReturn(house);
		ObjectMapper objectMapper = new ObjectMapper();
		String expected = objectMapper.writeValueAsString(house);
		String URI = "/api/house/{id}";
		String ID = "1";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI,ID).accept(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		String output = result.getResponse().getContentAsString();
		assertThat(expected).isEqualTo(output);
		
		
	}
	
	@Test
	public void testSaveHouseShouldThrowBadRequest() throws Exception{
		House house = new House("1",512,"Kathmandu",27,10000);
		Mockito.when(houseService.saveHouse(Mockito.any(House.class))).thenReturn(house);
		ObjectMapper objectMapper = new ObjectMapper();
		String input = objectMapper.writeValueAsString(house);
		String URI = "/api/house";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI)
										.accept(MediaType.APPLICATION_JSON)
										.content(input)
										.contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		assertEquals(HttpStatus.BAD_REQUEST.value(),result.getResponse().getStatus());
		
	}
	@Test
	public void testSaveHouse() throws Exception{
		House house = new House(512,"Kathmandu",27,10000);
		Mockito.when(houseService.saveHouse(Mockito.any(House.class))).thenReturn(house);
		ObjectMapper objectMapper = new ObjectMapper();
		String input = objectMapper.writeValueAsString(house);
		String URI = "/api/house";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post(URI)
										.accept(MediaType.APPLICATION_JSON)
										.content(input)
										.contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		String output = result.getResponse().getContentAsString();
		assertThat(output).isEqualTo(input);
		assertEquals(HttpStatus.OK.value(),result.getResponse().getStatus());
		
	}
	
	
	@Test
	public void testUpdateHouse() throws Exception{
		House house = new House("1",512,"Kathmandu",27,10000);
		House house2 = new House("1",514,"Kathmandu",28,12000);
		Mockito.when(houseService.getHouseById(Mockito.anyString())).thenReturn(house);
		ObjectMapper objectMapper = new ObjectMapper();
		String input = objectMapper.writeValueAsString(house2);
		String URI = "/api/house/{id}";
		String ID = "1";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.put(URI,ID)
										.accept(MediaType.APPLICATION_JSON)
										.content(input)
										.contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
	//	String output = result.getResponse().getContentAsString();
		Mockito.verify(houseService,Mockito.times(1)).saveHouse(Mockito.any(House.class));
		assertEquals(HttpStatus.OK.value(),result.getResponse().getStatus());
		
	}
	
	@Test
	public void testUpdateHouseShouldThrowNotFound() throws Exception{
		House house = new House("1",512,"Kathmandu",27,10000);
		Mockito.when(houseService.getHouseById(Mockito.anyString())).thenReturn(null);
		ObjectMapper objectMapper = new ObjectMapper();
		String input = objectMapper.writeValueAsString(house);
		String URI = "/api/house/{id}";
		String ID = "1";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.put(URI,ID)
										.accept(MediaType.APPLICATION_JSON)
										.content(input)
										.contentType(MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		Mockito.verify(houseService,Mockito.times(0)).saveHouse(Mockito.any(House.class));
		assertEquals(HttpStatus.NOT_FOUND.value(),result.getResponse().getStatus());
		
	}
	
	@Test
	public void testRemoveHouse() throws Exception{
		House house = new House("1",512,"Kathmandu",27,10000);
		Mockito.when(houseService.getHouseById(Mockito.anyString())).thenReturn(house);
		String URI = "/api/house/{id}";
		String ID = "1";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(URI,ID);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		Mockito.verify(houseService,Mockito.times(1)).removeHouse(house);
		assertEquals(HttpStatus.OK.value(),result.getResponse().getStatus());
	}
	@Test
	public void testRemoveHouseShouldThrowBadRequest() throws Exception{
		House house = new House("1",512,"Kathmandu",27,10000);
		Mockito.when(houseService.getHouseById(Mockito.anyString())).thenReturn(null);
		String URI = "/api/house/{id}";
		String ID = "1";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.delete(URI,ID);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		Mockito.verify(houseService,Mockito.times(0)).removeHouse(house);
		assertEquals(HttpStatus.NOT_FOUND.value(),result.getResponse().getStatus());
	}
}
