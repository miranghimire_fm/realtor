package com.fm.realtor.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.fm.realtor.model.House;
import com.fm.realtor.repository.HouseRepository;
@RunWith(SpringRunner.class)
@SpringBootTest
public class HouseServiceTest {
	@Autowired
	private HouseService houseService;
	
	@MockBean
	private HouseRepository houseRepository;
	
	@Test
	public void testGetAllHouse() {
		List<House> house = new ArrayList<>();
		house.add(new House("1",512,"Kathmandu",27,10000));
		Mockito.when(houseRepository.findAll()).thenReturn(house);
		List<House> result = houseService.getAllHouse();
		assertEquals(1,result.size());
	}
	
	@Test
	public void testSaveHouse() {
		House house = new House("1",521,"Patan",28,12000);
		Mockito.when(houseRepository.save(Mockito.any(House.class))).thenReturn(house);
		House result = houseService.saveHouse(house);
		assertEquals("1",result.getId());
	}
	
	@Test
	public void testRemoveHouse() {
		House house = new House("1", 521, "Lalitpur", 29, 130000);
		houseService.removeHouse(house);
		Mockito.verify(houseRepository,Mockito.times(1)).delete(house);
		
	}
	@Test
	public void testGetHouseById() {
		List<House> house = new ArrayList<>();
		house.add(new House("1",512,"Bhaktapur",27,15000));
		house.add(new House("2",512,"Lumbini",28,17000));
		Mockito.when(houseRepository.findOne("2")).thenReturn(house.get(1));
		House result = houseService.getHouseById("2");
		assertEquals("2",result.getId());
		
	}
	
	
}
