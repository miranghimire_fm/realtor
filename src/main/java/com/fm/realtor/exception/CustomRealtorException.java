package com.fm.realtor.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

public class CustomRealtorException extends Exception{
	private static final long serialVersionUID = 1L;
	private String errorMessage;
	private HttpStatus httpStatus;
	private HttpHeaders httpHeaders;
	
	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

	public CustomRealtorException(String errorMessage, HttpStatus httpStatus, HttpHeaders httpHeaders) {
		super(errorMessage);
		//this.errorMessage = errorMessage;
		//this.errorCode = errorCode;
		this.httpStatus = httpStatus;
		this.httpHeaders = httpHeaders;
	}

	public HttpHeaders getHttpHeaders() {
		return httpHeaders;
	}

	public void setHttpHeaders(HttpHeaders httpHeaders) {
		this.httpHeaders = httpHeaders;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

}
