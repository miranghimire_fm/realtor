package com.fm.realtor.exception;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

@RestControllerAdvice
public class RestExceptionHandler {

	@ExceptionHandler(CustomRealtorException.class)
	public ResponseEntity<ErrorResponse> globalExceptionHandler(CustomRealtorException e){
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setErrorCode(e.getHttpStatus().value());
		errorResponse.setMessage(e.getMessage());
		return new ResponseEntity<ErrorResponse>(errorResponse,e.getHttpStatus());
		
	}
	
	@ExceptionHandler(NoHandlerFoundException.class)
	@ResponseStatus(value=HttpStatus.NOT_FOUND)
	public ResponseEntity<ErrorResponse> exceptionRealtorHandler(HttpServletRequest req,NoHandlerFoundException e){
		ErrorResponse errorResponse = new ErrorResponse();
		HttpHeaders headers = e.getHeaders();
		errorResponse.setErrorCode(HttpStatus.NOT_FOUND.value());
		errorResponse.setMessage(e.getMessage());
		return new ResponseEntity<ErrorResponse>(errorResponse,headers,HttpStatus.NOT_FOUND);
		
	} 
/*	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(HttpStatus.BAD_REQUEST.value());
		error.setMessage("The request could not be understood by the server due to malformed syntax.");
		return new ResponseEntity<ErrorResponse>(error, HttpStatus.BAD_REQUEST);
}*/
//2nd way to throw the exception	
	/*@ExceptionHandler(NoHandlerFoundException.class)
	@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="content not found")
	public void conflict(){
		
	}*/
//3rd way to throw the exception
/*	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(NoHandlerFoundException.class)
	@ResponseBody ErrorInfo
	handleBadRequest(Exception ex) {
	    return new ErrorInfo(ex);
	}*/ 
}
