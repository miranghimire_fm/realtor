package com.fm.realtor;


import java.util.List;

import org.ejml.simple.SimpleMatrix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

import com.fm.realtor.Regression.Regression;
import com.fm.realtor.model.House;
import com.fm.realtor.repository.HouseRepository;


@SpringBootApplication
@RestController
public class RealtorApplication implements CommandLineRunner{
	

	@Autowired 
	private Regression regression;
	@Autowired
	private HouseRepository houseRepository;
	public static void main(String[] args) {
		
	//	LinearRegression lr = new LinearRegression();
	//	System.out.println(lr.hello());
	//	 ConfigurableApplicationContext ctx = 
	SpringApplication.run(RealtorApplication.class, args);
	//	 LinearRegression bean = ctx.getBean(LinearRegression.class);
	//	 System.out.println(bean.train());
		
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		if(regression.checkIfTrained()) {
			System.out.println("Model has been trained");
		}else {
			System.out.println("Training Initiated");
			List<House> house = houseRepository.findAll();
			double ratio = 0.6;
			int dimension = 2;
			List<SimpleMatrix> data = regression.train_test_split(house, ratio, dimension);
			SimpleMatrix x_train = data.get(0);
			SimpleMatrix y_train = data.get(1);
			SimpleMatrix x_test = data.get(2);
			SimpleMatrix y_test = data.get(3);
		/*	x_train.print();
			y_train.print();
			x_test.print();
			y_test.print(); */
			regression.train(x_train,y_train);
		//	regression.train();
		}
		
	}
}
