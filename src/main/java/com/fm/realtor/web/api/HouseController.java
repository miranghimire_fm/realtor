package com.fm.realtor.web.api;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.fm.realtor.exception.CustomRealtorException;
import com.fm.realtor.model.House;
import com.fm.realtor.service.HouseService;

@RestController
@RequestMapping("/api")
public class HouseController {
	@Autowired
	private HouseService houseService;
	
	@RequestMapping(value="/",
					method=RequestMethod.GET,
					produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> hello() {
		String response = "{\"response\":\"hello to this you\"}";
		return new ResponseEntity<Object>(response,HttpStatus.OK);
	}
	@RequestMapping(value="/house",
			method=RequestMethod.GET,
			produces=MediaType.APPLICATION_JSON_VALUE)	
	public ResponseEntity<List<House>> getAllHouse() throws CustomRealtorException{
		List<House> house = houseService.getAllHouse();
		if(house == null)
			throw new CustomRealtorException("Content not found",HttpStatus.NOT_FOUND,null);
			
		return new ResponseEntity<List<House>>(house,HttpStatus.OK);
	
	}

	@RequestMapping(value="/house/{id}",
			method=RequestMethod.GET,
			produces=MediaType.APPLICATION_JSON_VALUE)	
	public ResponseEntity<?> getHouseById(@PathVariable ("id") String id) throws CustomRealtorException, NoHandlerFoundException{
		House house = houseService.getHouseById(id);
		HttpHeaders headers = new HttpHeaders();
		headers.add("content", "not found");
		if(house == null)
			//headers.add("Content","Not found");
			//return new ResponseEntity<CustomRealtorException>(new CustomRealtorException("Content not found"),headers,HttpStatus.NOT_FOUND);
			//throw new CustomRealtorException("Content not found");

			throw new NoHandlerFoundException("id",id,headers);
			//throw new NoHandlerFoundException(null, null, null);
			//throw new CustomRealtorException("Content not found",HttpStatus.NOT_FOUND,null);
		return new ResponseEntity<House>(house,HttpStatus.OK);
	}

	@RequestMapping(value="/house/{id}",
			method=RequestMethod.DELETE)
	public ResponseEntity<Object> removeHouse(@PathVariable ("id") String id) throws CustomRealtorException{
		House house = houseService.getHouseById(id);
		if(house == null)
			throw new CustomRealtorException("Content does not exists",HttpStatus.NOT_FOUND,null);	
		houseService.removeHouse(house);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	@RequestMapping(value="/house",
			method=RequestMethod.POST,
			produces=MediaType.APPLICATION_JSON_VALUE,
			consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<House> saveHouse(@RequestBody House house) throws CustomRealtorException{
		String id = house.getId();
		//House houseO = houseService.getHouseById(id);
		if(id == null)
			return new ResponseEntity<House>(houseService.saveHouse(house),HttpStatus.OK);
		else
			throw new CustomRealtorException("Content already exists",HttpStatus.BAD_REQUEST,null);
	}	
	@RequestMapping(value="/house/{id}",
			method=RequestMethod.PUT,
			produces=MediaType.APPLICATION_JSON_VALUE,
			consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<House> updateHouse (@PathVariable("id") String id, @RequestBody House house)throws CustomRealtorException{
		House houseO = houseService.getHouseById(id);
		if(houseO == null)
			throw new CustomRealtorException("Content doesn't exits",HttpStatus.NOT_FOUND,null);
		return new ResponseEntity<House>(houseService.saveHouse(house),HttpStatus.OK);
	}	
	@RequestMapping(value="/train",
			produces=MediaType.APPLICATION_JSON_VALUE)
	public String train() {
		return houseService.train();
	}
}

