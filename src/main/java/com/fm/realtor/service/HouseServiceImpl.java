package com.fm.realtor.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fm.realtor.Regression.LinearRegression;
import com.fm.realtor.model.House;
import com.fm.realtor.repository.HouseRepository;

@Service
public class HouseServiceImpl implements HouseService{

	@Autowired
	private HouseRepository houserepository;
	@Autowired
	private LinearRegression linearRegression;
	@Override
	public List<House> getAllHouse() {
		List<House> listHouse = houserepository.findAll();
		if(!listHouse.isEmpty())
			return listHouse;
		else
			return null;
	}

	@Override
	public House getHouseById(String id) {
		return houserepository.findOne(id);
		
	}

	@Override
	public House saveHouse(House house) {
		return houserepository.save(house);
	}

	@Override
	public void removeHouse(House house) {
		houserepository.delete(house);
	}

	@Override
	public String train() {
		return linearRegression.train();
	}

}
