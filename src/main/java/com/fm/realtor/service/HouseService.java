package com.fm.realtor.service;

import java.util.List;

import com.fm.realtor.model.House;

public interface HouseService {
	public List<House> getAllHouse();
	public House getHouseById(String id);
	public House saveHouse(House house);
	public void removeHouse(House house);
	public String train();
}
