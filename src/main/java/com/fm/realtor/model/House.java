package com.fm.realtor.model;



import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "house")
public class House {
	@Id
	String id;
	float area;
	String location;
	float distance_from_ktm;
	float price;
	
	public House(float area, String location, float distance_from_ktm, float price) {
		super();
		this.area = area;
		this.location = location;
		this.distance_from_ktm = distance_from_ktm;
		this.price = price;
	}
	public House() {
		super();
	}
	public House(String id, float area, String location, float distance_from_ktm, float price) {
		super();
		this.id = id;
		this.area = area;
		this.location = location;
		this.distance_from_ktm = distance_from_ktm;
		this.price = price;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public float getArea() {
		return area;
	}
	public void setArea(float area) {
		this.area = area;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public float getDistance_from_ktm() {
		return distance_from_ktm;
	}
	public void setDistance_from_ktm(float distance_from_ktm) {
		this.distance_from_ktm = distance_from_ktm;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
}
