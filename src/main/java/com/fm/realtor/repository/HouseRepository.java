package com.fm.realtor.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.fm.realtor.model.House;

public interface HouseRepository extends MongoRepository<House, String> {
//	public House findHouseById(String id);
}
