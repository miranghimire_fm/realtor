package com.fm.realtor.Regression;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.ejml.simple.SimpleMatrix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fm.realtor.model.House;
import com.fm.realtor.repository.HouseRepository;

@Component
public class LinearRegression implements Regression{
	
	@Autowired
	private HouseRepository houserepository;
	SimpleMatrix inputs;
	List<SimpleMatrix> testSet;
	List<SimpleMatrix> trainSet;
	SimpleMatrix weights;
	SimpleMatrix actual_output;
	SimpleMatrix predicted_output;

	
	public Boolean checkIfTrained() {
		try {
			SimpleMatrix.loadBinary("matrix_file.data");
			return true;
		}catch(IOException e){
			return false;
		}
	}
	public String train() {
		try {
			SimpleMatrix B = SimpleMatrix.loadBinary("matrix_file.data");
			B.print();
			} catch (IOException e) {
				List<House> house = houserepository.findAll();
				int datasetSize = house.size();
				System.out.println(datasetSize);
				
				SimpleMatrix x = new SimpleMatrix(60,3);
				SimpleMatrix y = new SimpleMatrix(60,1);
					
				int i = 0;
				for(int j=0; j<60; ++j) {
					x.set(j,0,1);
				}
				
				for(House h: house) {

				//	System.out.println(h.getId());
					x.set(i,1,h.getArea());
					x.set(i,2,h.getDistance_from_ktm());
					y.set(i,0,h.getPrice());
					i++; 
					if (i==60)
						break;
			}
				//y.print();
				//x.print();
					
				SimpleMatrix w = new SimpleMatrix(3,1);
				w = (x.transpose().mult(x)).invert().mult(x.transpose()).mult(y);
				w.print();
				try {
					w.saveToFileBinary("matrix_file.data");
					} catch (IOException ex) {
						System.out.println("File couldn't be saved");
				}
				
				String response = "{\"response\":\"Hello!!!  Train Sucessful..........\"}";
				return response;
				
		}
		String response = "{\"response\":\"Hello!!! Weights retrieved from saved state..........\"}";
		return response;
	
	}
	@Override
	public SimpleMatrix train(SimpleMatrix x_train, SimpleMatrix y_train) {
		SimpleMatrix weights = new SimpleMatrix(x_train.numCols(),1);
		weights = (x_train.transpose().mult(x_train)).invert().mult(x_train.transpose()).mult(y_train);
		weights.print();
		SimpleMatrix predicted = new SimpleMatrix(x_train.numRows(),1);
		predicted = (x_train).mult(weights);
		predicted.print();
		y_train.print();
		return weights;
	}
	@Override
	public String test(SimpleMatrix acutal, SimpleMatrix predicted) {
		// TODO Auto-generated method stub
		return null;
	}
	//public String calculate
	@Override
	public float predict(float area, float distance) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public List<SimpleMatrix> train_test_split(List<House> house, double ratio, int dimension) {
		// TODO Auto-generated method stub
		int size = house.size();
		int trainSize = (int) (size * ratio);
		int testSize = size - trainSize;
		System.out.println(trainSize);
		SimpleMatrix x_train = new SimpleMatrix(trainSize,dimension+1);
		SimpleMatrix y_train = new SimpleMatrix(trainSize,1);
		SimpleMatrix x_test = new SimpleMatrix(testSize, dimension+1);
		SimpleMatrix y_test = new SimpleMatrix(testSize,1);			
		int i = 0;
		int j = 0;
		//for(int j=0; j<trainSize; ++j) {
		//	x_train.set(j,0,1);
		//}
		
		for(House h: house) {

		//	System.out.println(h.getId());
			if( i< trainSize) {
		//	System.out.println("hello");
			x_train.set(i,0,1);
			x_train.set(i,1,h.getArea());
			x_train.set(i,2,h.getDistance_from_ktm());
			y_train.set(i,0,h.getPrice());
			}else {
				x_test.set(j,0,1);
				x_test.set(j,1,h.getArea());
				x_test.set(j,2,h.getDistance_from_ktm());
				y_test.set(j,0,h.getPrice());
				j++;
			}
			i++; 
	}
		//System.out.println("hello");
		//x_train.print();
		//x_test.print();
		//y_train.print();
		//y_test.print();
		//List<SimpleMatrix> list = new List<SimpleMatrix>();
		List<SimpleMatrix>list = new ArrayList<SimpleMatrix>(Arrays.asList(x_train,y_train,x_test,y_test));
		return list;
	}

}
