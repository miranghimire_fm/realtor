package com.fm.realtor.Regression;

import java.util.List;

import org.ejml.simple.SimpleMatrix;

import com.fm.realtor.model.House;

public interface Regression {
	public SimpleMatrix train(SimpleMatrix x_train, SimpleMatrix y_train);
	public String train();
	public Boolean checkIfTrained();
	public String test(SimpleMatrix acutal, SimpleMatrix predicted);
	public float predict(float area, float distance);
	public List<SimpleMatrix> train_test_split(List<House> house, double ratio, int dimension);

}
